# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

#
# Angularアプリの設定
#
cartExample = angular.module('cartExample', ['ngResource'])

#
# Angularがturbolinksの影響で画面遷移後に発動しない場合があるのを解決する
#
$(document).on('ready page:load', ->
  angular.bootstrap(document.body, ['cartExample'])
)

#
# Angularを使ってリクエストを送った時にcsrf-tokenが正常に送られない件を解決する
#
cartExample.config ($httpProvider) ->
  authToken = angular.element('meta[name="csrf-token"]').attr("content");
  $httpProvider.defaults.headers.common["X-CSRF-TOKEN"] = authToken
  $httpProvider.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";

#
# /cartsへのアクセスをするリソースを定義
#
cartExample.factory 'Cart', ['$resource', ($resource) ->
  return $resource '/cart/index.json', {}, {
    query: {method:'GET', isArray:true}}]

#
# /users配下で動くコントローラーを定義
#
cartExample.controller 'CartController', ($scope, Cart) ->
  $scope.carts = Cart.query()
  $scope.buttonDisabled = true;

  #
  # 削除に対するアクション
  #
  $scope.deleteButton = (index) ->
    if !confirm("本当にキャンセルしてよいですか？")
      return

    cart = $scope.carts[index]

    cart.$remove({id: cart.id})

    $scope.carts.splice(index, 1)

    $scope.buttonDisabled = true;
