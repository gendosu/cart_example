json.array!(@cart.cart_items) do |cart_item|
  json.extract! cart_item, :sku, :product_item_id, :quantity
  product_item = ProductItem.find_by(sku: cart_item.sku)
  json.extract! product_item, :thumbnail_url, :sales_price
end
