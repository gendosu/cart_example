# To change this license header, choose License Headers in Project Properties. To change this template file, choose
# Tools | Templates and open the template in the editor.

class Cart
  include ActiveModel::Model

  attr_accessor :cart_items

  def initialize
    @cart_items ||= []
  end

  # カートインスタンスに商品と個数を追加します
  def add_item(sku, quantity)
    cart_item = @cart_items.find{|a| a.sku.eql? sku}
    unless cart_item
      cart_item = CartItem.new
      cart_item.sku = sku
      @cart_items << cart_item
    end
    cart_item.quantity += quantity.to_i

    cart_item
  end

  # カートインスタンスに商品と個数を変更します
  def edit_item(sku, quantity)
    cart_item = @cart_items.find{|a| a.sku.eql? sku}
    if cart_item
      cart_item = CartItem.new
      cart_item.quantity = quantity
    end
  end

  # カートインスタンスに商品と個数を追加します
  def remove_item(sku)
    @cart_items.delete_if{|a|
      a.sku.eql? sku
    }
  end
end
