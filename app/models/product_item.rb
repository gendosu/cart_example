class ProductItem < ActiveRecord::Base
  belongs_to :cart
  belongs_to :product

  def selectable_quantity
    10
  end
end
