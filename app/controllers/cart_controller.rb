class CartController < ApplicationController

  before_action :authenticate_user!

  before_action :get_cart

  def index
  end

  # カートに商品を追加する
  def add_cart
    cart_item = @cart.add_item(add_cart_params[:sku], add_cart_params[:quantity])
    redirect_to cart_index_path

    flash[:cart_item] = cart_item
    session[:cart] = @cart
  end

  # カートに商品を追加する
  def edit_cart
    cart_item = @cart.edit_item(edit_cart_params[:sku], edit_cart_params[:quantity])
    redirect_to cart_index_path

    flash[:cart_item] = cart_item
    session[:cart] = @cart
  end

  # カート内の商品を削除する
  def remove_cart
    cart_item = @cart.add_item(add_cart_params[:sku], add_cart_params[:quantity])
    redirect_to cart_index_path

    flash[:cart_item] = cart_item
    session[:cart] = @cart
  end

  def clear_cart
    session[:cart] = nil

    redirect_to product_items_path
  end

  private

    # カートに追加する時に必要なパラメータ
    def add_cart_params
      params.require(:cart_item).permit(:sku, :quantity)
    end

    # カートに追加する時に必要なパラメータ
    def edit_cart_params
      params.require(:cart_item).permit(:sku, :quantity)
    end

    # カートから商品を削除する時に必要なパラメータ
    def remove_cart_params
      params.require(:cart_item).permit(:sku)
    end
end
