require "rails_helper"

RSpec.describe ProductItemsController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/product_items").to route_to("product_items#index")
    end

    it "routes to #new" do
      expect(:get => "/product_items/new").to route_to("product_items#new")
    end

    it "routes to #show" do
      expect(:get => "/product_items/1").to route_to("product_items#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/product_items/1/edit").to route_to("product_items#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/product_items").to route_to("product_items#create")
    end

    it "routes to #update" do
      expect(:put => "/product_items/1").to route_to("product_items#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/product_items/1").to route_to("product_items#destroy", :id => "1")
    end

  end
end
