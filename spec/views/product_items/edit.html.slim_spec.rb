require 'rails_helper'

RSpec.describe "product_items/edit", :type => :view do
  before(:each) do
    @product_item = assign(:product_item, ProductItem.create!(
      :product_id => 1,
      :sku => "MyString",
      :sales_price => "MyString"
    ))
  end

  it "renders the edit product_item form" do
    render

    assert_select "form[action=?][method=?]", product_item_path(@product_item), "post" do

      assert_select "input#product_item_product_id[name=?]", "product_item[product_id]"

      assert_select "input#product_item_sku[name=?]", "product_item[sku]"

      assert_select "input#product_item_sales_price[name=?]", "product_item[sales_price]"
    end
  end
end
