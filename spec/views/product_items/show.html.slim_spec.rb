require 'rails_helper'

RSpec.describe "product_items/show", :type => :view do
  before(:each) do
    @product_item = assign(:product_item, ProductItem.create!(
      :product_id => 1,
      :sku => "Sku",
      :sales_price => "Sales Price"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/1/)
    expect(rendered).to match(/Sku/)
    expect(rendered).to match(/Sales Price/)
  end
end
