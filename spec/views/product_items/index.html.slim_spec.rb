require 'rails_helper'

RSpec.describe "product_items/index", :type => :view do
  before(:each) do
    assign(:product_items, [
      ProductItem.create!(
        :product_id => 1,
        :sku => "Sku",
        :sales_price => "Sales Price"
      ),
      ProductItem.create!(
        :product_id => 1,
        :sku => "Sku",
        :sales_price => "Sales Price"
      )
    ])
  end

  it "renders a list of product_items" do
    render
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Sku".to_s, :count => 2
    assert_select "tr>td", :text => "Sales Price".to_s, :count => 2
  end
end
