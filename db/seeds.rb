# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

FactoryGirl.create(:user, email: "gendosu@gmail.com", password: "gen123")

product = FactoryGirl.create(:product, code: "k", name: "消しゴム")
FactoryGirl.create(:product_item, product: product, sku: "k-01", sales_price: 100)

product = FactoryGirl.create(:product, code: "134584-08", name: "ＭＥＮ　ウルトラライトダウンハーフコート（プリント）")
FactoryGirl.create(:product_item, product: product, sku: "134584-08-004", sales_price: 6900, image_url: "http://im.uniqlo.com/images/jp/pc/goods/134584/item/05_134584.jpg", thumbnail_url: "http://im.uniqlo.com/images/jp/pc/goods/134584/item/05_134584_small.jpg")
FactoryGirl.create(:product_item, product: product, sku: "134584-08-003", sales_price: 6900, image_url: "http://im.uniqlo.com/images/jp/pc/goods/134584/item/08_134584.jpg", thumbnail_url: "http://im.uniqlo.com/images/jp/pc/goods/134584/item/08_134584_small.jpg")
