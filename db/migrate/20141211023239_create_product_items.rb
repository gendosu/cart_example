class CreateProductItems < ActiveRecord::Migration
  def change
    create_table :product_items do |t|
      t.integer :product_id, :null => false
      t.string :sku,         :null => false
      t.string :sales_price, :null => false
      t.string :image_url
      t.string :thumbnail_url

      t.timestamps
    end
  end
end
